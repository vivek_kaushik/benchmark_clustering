# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Lumity Product Management and Development Teams 
* Contains 2 differnet ML algorithms to cluster Lumity's clients given their plan characterstic information.
* Version 0.0

### How do I get set up? ###

* Download and unzip the repository onto your local machine
* Install latest versions of Python 3.x and Jupyter Notebook
* Open Command Prompt on Windows or Terminal on Mac and change directory to the location of the repository
* Open any one of the .ipynb notebooks
* Make sure in notebooks, the cells with import statements work. If a library is not installed, type '!pip3 install <insert missing library name>'

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner: Vivek Kaushik (vivek.kaushik@lumity.com)
* Other community or team contact: Ruey Pham (ruey@lumity.com), Udara Bentota (udara@lumity.com), Lasitha Ishan (lasitha@lumity.com), Sivakumar Chandrasekharan (siva@lumity.com)